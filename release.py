#!/usr/bin/env python3

import os
import sys
import shutil
from string import Template
from pathlib import Path
from datetime import datetime
from subprocess import check_call, DEVNULL
import hashlib

import requests

import woob
from woob.core.modules import ModulesLoader


base_path = Path(os.path.dirname(__file__))
packages_root = base_path / 'packages'
modules_dir = base_path / 'modules'


def get_tree_mtime(path, include_root=False):
    mtime = 0
    if include_root or not os.path.isdir(path):
        mtime = int(datetime.fromtimestamp(os.path.getmtime(path)).strftime('%Y%m%d%H%M'))
    for root, _, files in os.walk(path):
        for f in files:
            if f.endswith('.pyc'):
                continue
            m = int(datetime.fromtimestamp(os.path.getmtime(os.path.join(root, f))).strftime('%Y%m%d%H%M'))
            mtime = max(mtime, m)

    return mtime


class Package:
    def __init__(self, package_name):
        self.package_name = package_name
        self.dist_dir = packages_root / 'dist' / self.package_name

    def build(self):
        check_call(
            [
                sys.executable,
                '-m', 'build',
                '-o', self.dist_dir,
                packages_root / self.package_name,
            ],
            stdout=DEVNULL,
            stderr=DEVNULL,
        )


class Module:
    def __init__(self, module_name, modules_loader):
        self.modules_loader = modules_loader
        self.module_name = module_name
        self.package_name = f'woob-module-{module_name}'
        self.package_dir = packages_root / self.package_name
        self.package = Package(self.package_name)

        self.module_version = get_tree_mtime(modules_dir / self.module_name)

        repository = os.environ['TWINE_REPOSITORY_URL']

        self.tgz_url = f'{repository}/{self.package_name}/{self.package_name}-{self.module_version}.tar.gz'
        self.whl_url = f'{repository}/{self.package_name}/{self.package_name.replace("-", "_")}-{self.module_version}-py3-none-any.whl'

        self.requirement = f'{self.package_name} @ {self.whl_url}'

    def is_latest_version(self):
        r = requests.head(self.tgz_url, timeout=1)
        return r.status_code == 200

    def create_package(self, pyproject_template):
        self.copy()
        self.render_pyproject(pyproject_template)

    def copy(self):
        # Copy module in the package directory.
        module_dir = self.package_dir / 'woob_modules' / self.module_name

        os.makedirs(self.package_dir / 'woob_modules')
        shutil.copytree(modules_dir / self.module_name, module_dir)

        # package files are copied in the package directory.
        if os.path.exists(module_dir / 'requirements.txt'):
            shutil.move(str(module_dir / 'requirements.txt'), self.package_dir)
        if os.path.exists(module_dir / 'LICENSE'):
            shutil.move(str(module_dir / 'LICENSE'), self.package_dir)
        if not os.path.exists(module_dir / 'README.md'):
            (module_dir / 'README.md').touch()

        # woob_modules is a real (implicite namespace) module
        (self.package_dir / 'woob_modules' / '__init__.py').touch()

    def render_pyproject(self, template):
        m = self.modules_loader.get_or_load_module(self.module_name)
        module_description = m.description

        pyproject = template.substitute(
            {'MODULE_NAME': self.package_name,
             'MODULE_VERSION': self.module_version,
             'MODULE_DESCRIPTION': module_description.replace('"', '\\"').split('\n')[0],
             'MODULE_MAINTAINER': m.klass.MAINTAINER,
             'MODULE_EMAIL': m.klass.EMAIL,
             'MODULE_LICENSE': m.license,
            }
        )
        (self.package_dir / 'pyproject.toml').write_text(pyproject)

    def add_local_requirement(self):
        return
        for filename in os.listdir(self.package.dist_dir):
            filehash = hashlib.sha256((self.package.dist_dir / filename).read_bytes()).hexdigest()
            self.requirement += f' --hash=sha256:{filehash}'

    def add_remote_requirement(self):
        return
        for url in (self.tgz_url, self.whl_url):
            r = requests.get(url, timeout=1)
            filehash = hashlib.sha256(r.content).hexdigest()
            self.requirement += f' --hash=sha256:{filehash}'


class Builder:
    def __init__(self):
        self.modules_loader = ModulesLoader(str(modules_dir), woob.__version__)

        self.pyproject_template = None
        self.requirements = []

    def prepare(self):
        try:
            shutil.rmtree(packages_root)
        except FileNotFoundError:
            pass

        os.makedirs(packages_root)

        self.pyproject_template = Template((base_path / 'pyproject.toml.tmpl').read_text())

    def main(self):
        self.prepare()

        for module_name in os.listdir(modules_dir)[:3]:
            if module_name.startswith('.'):
                continue

            print(module_name, end='... ', flush=True)

            module = Module(module_name, self.modules_loader)

            if not module.is_latest_version():
                module.create_package(self.pyproject_template)

                module.package.build()
                print('built.')

                module.add_local_requirement()
            else:
                module.add_remote_requirement()
                print('already last version')

            self.requirements.append(module.requirement)

        self.build_meta_package()

    def build_meta_package(self):
        print('woob-modules', end='... ', flush=True)
        woob_modules_path = packages_root / 'woob-modules'
        os.makedirs(woob_modules_path)

        package = Package('woob-modules')

        pyproject = self.pyproject_template.substitute(
            {'MODULE_NAME': 'woob-modules',
             'MODULE_VERSION': datetime.now().strftime('%Y%m%d%H%M'),
             'MODULE_DESCRIPTION': 'All official modules of woob',
             'MODULE_MAINTAINER': 'Romain Bignon',
             'MODULE_EMAIL': 'romain@bignon.me',
             'MODULE_LICENSE': 'LGPL3',
            }
        )
        (woob_modules_path / 'pyproject.toml').write_text(pyproject)
        (woob_modules_path / 'requirements.txt').write_text('\n'.join(self.requirements) + '\n')

        package.build()
        print('built.')



if __name__ == '__main__':
    Builder().main()
